all: clean build run

clean:
	@rm -rf bin

build:
	-@mkdir bin
	@mcs ${FLAGS} -out:bin/mc.exe -recurse:'MidnightCommanderClone/*.cs'

run:
	@cd bin; mono ${FLAGS} mc.exe

debug: clean build-debug run-debug

build-debug: FLAGS=-debug
build-debug: build
run-debug: FLAGS=--debug
run-debug: run

lsc:
	@echo
	@printf "%s classes found:\n" `grep -r "class" MidnightCommanderClone | wc -l`
	@grep -r "class" MidnightCommanderClone | sed "s/: */\&/;s/ class / class \&/;s/ : /\&/;s/ {.*//" | column -c 1 -s "&" -N File,Signature,Name,Extends --table
	@echo
