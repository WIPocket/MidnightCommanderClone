using System;

namespace MidnightCommanderClone {
  public class ConsoleBuffer {
    public ConsoleCharacter[,] Buffer = new ConsoleCharacter[0,0];
    private ConsoleCharacter[,] Rendered = new ConsoleCharacter[0,0];

    public int Width { get; private set; } = 0;
    public int Height { get; private set; } = 0;

    private bool ShouldShowCursor = false;
    private (int x, int y) CursorPosition;

    public void Flush() {
      var needToMoveCursor = true;
      ConsoleColor lastForeground = MidnightCommanderClone.Foreground;
      Console.ForegroundColor = lastForeground;
      ConsoleColor lastBackground = MidnightCommanderClone.Background;
      Console.BackgroundColor = lastBackground;
      Console.CursorVisible = false;

      try {
        for (int y = 0; y < Height; y++) {
          for (int x = 0; x < Width; x++) {
            var _needToMoveCursor = needToMoveCursor;
            needToMoveCursor = true;
            var target = Buffer[x, y];
            if (target == null) continue;
            var current = Rendered[x, y];
            if (current == null) current = new ConsoleCharacter(' ', ConsoleColor.White, MidnightCommanderClone.Background);
            if (target == current) continue;

            needToMoveCursor = false;
            if (_needToMoveCursor) Console.SetCursorPosition(x, y);
            if (lastForeground != target.Foreground) Console.ForegroundColor = target.Foreground;
            lastForeground = target.Foreground;
            if (lastBackground != target.Background) Console.BackgroundColor = target.Background;
            lastBackground = target.Background;

            Console.Write(target.Character);
            Rendered[x, y] = target.Clone();
          }
        }

        if(ShouldShowCursor) {
          Console.SetCursorPosition(CursorPosition.x, CursorPosition.y);
          Console.CursorVisible = true;
          ShouldShowCursor = false;
        }
      } catch {}
    }

    public void Resize(int width, int height) {
      if((int)this.Width == width && (int)this.Height == height) return;
      this.Width = width;
      this.Height = height;
      this.Buffer = new ConsoleCharacter[width, height];
      this.Rendered = new ConsoleCharacter[width, height];
      Console.BackgroundColor = MidnightCommanderClone.Background;
      Console.Clear();
    }

    public void ShowCursor(int x, int y) {
      ShouldShowCursor = true;
      CursorPosition = (x, y);
    }
  }
}
