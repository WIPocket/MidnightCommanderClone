namespace MidnightCommanderClone {
  public abstract class ConsoleUnit {
    public int Size;
    public ConsoleUnits Unit;

    public static implicit operator ConsoleUnit(int size) {
      return new Characters() { Size = size, Unit = ConsoleUnits.Characters };
    }
  }

  public class Characters : ConsoleUnit {
    public static implicit operator Characters(int size) {
      return new Characters() { Size = size, Unit = ConsoleUnits.Characters };
    }
    
    public static implicit operator int(Characters unit) {
      return unit.Size;
    }
  }

  public class Ratio : ConsoleUnit {
    public static explicit operator Ratio(int size) {
      return new Ratio() { Size = size, Unit = ConsoleUnits.Ratio };
    }
  }

  public enum ConsoleUnits {
    Characters,
    Ratio
  }
}
