using System;

namespace MidnightCommanderClone {
  public class ConsoleCharacter {
    public char Character;
    public ConsoleColor Foreground;
    public ConsoleColor Background;

    public ConsoleCharacter(char character, ConsoleColor foreground, ConsoleColor background) {
      this.Character = character;
      this.Foreground = foreground;
      this.Background = background;
    }

    public ConsoleCharacter(char character) {
      this.Character = character;
      this.Foreground = MidnightCommanderClone.Foreground;
      this.Background = MidnightCommanderClone.Background;
    }

    public ConsoleCharacter Clone() {
      return new ConsoleCharacter(this.Character, this.Foreground, this.Background);
    }

    public static bool operator ==(ConsoleCharacter left, ConsoleCharacter right) {
      if (object.ReferenceEquals(null, left) || object.ReferenceEquals(null, right))
        return object.ReferenceEquals(left, right);

      return left.Character  == right.Character
          && left.Foreground == right.Foreground
          && left.Background == right.Background;
    }
    public static bool operator !=(ConsoleCharacter left, ConsoleCharacter right) {
      return !(left == right);
    }
  }
}
