using System;

namespace MidnightCommanderClone {
  public static class Keys {
    public static ConsoleKey[] Help { get { return KeySet.Help; } }
    public static ConsoleKey[] Menu { get { return KeySet.Menu; } }
    public static ConsoleKey[] View { get { return KeySet.View; } }
    public static ConsoleKey[] Edit { get { return KeySet.Edit; } }
    public static ConsoleKey[] Copy { get { return KeySet.Copy; } }
    public static ConsoleKey[] RenMov { get { return KeySet.RenMov; } }
    public static ConsoleKey[] Mkdir { get { return KeySet.Mkdir; } }
    public static ConsoleKey[] Delete { get { return KeySet.Delete; } }
    public static ConsoleKey[] Touch { get { return KeySet.Touch; } }
    public static ConsoleKey[] Quit { get { return KeySet.Quit; } }
    public static ConsoleKey[] Up { get { return KeySet.Up; } }
    public static ConsoleKey[] Down { get { return KeySet.Down; } }
    public static ConsoleKey[] PgUp { get { return KeySet.PgUp; } }
    public static ConsoleKey[] PgDown { get { return KeySet.PgDown; } }
    public static ConsoleKey[] Tab { get { return KeySet.Tab; } }
    public static ConsoleKey[] Left { get { return KeySet.Left; } }
    public static ConsoleKey[] Right { get { return KeySet.Right; } }
    public static ConsoleKey[] Backspace { get { return KeySet.Backspace; } }
    public static ConsoleKey[] Save { get { return KeySet.Save; } }

    public static KeySet KeySet;

    public static bool Is(ConsoleKeyInfo info, ConsoleKey[] action) {
      if(action == null) return false;
      for(int i = 0; i < action.Length; i++)
        if(action[i] == info.Key) return true;
      return false;
    }
  }

  public abstract class KeySet {
    public string[] HelpLine;
    public ConsoleKey[] Help, Menu, View, Edit, Copy, RenMov,
      Mkdir, Delete, Touch, Quit, Up, Down, Left, Right, PgUp, PgDown, Tab,
      UnWrap, Hex, Goto, Search, Raw, Format, Backspace,
      Save, Mark, Replace, Move;
  }

  public class FileExplorerKeys : KeySet {
    public FileExplorerKeys() {
      HelpLine = new string[] { "Help", "Menu", "View", "Edit", "Copy", "RenMov", "Mkdir", "Delete", "Touch", "Quit" };
      Help = new ConsoleKey[] { ConsoleKey.F1 };
      Menu = new ConsoleKey[] { ConsoleKey.F2 };
      View = new ConsoleKey[] { ConsoleKey.F3, ConsoleKey.Enter };
      Edit = new ConsoleKey[] { ConsoleKey.F4 };
      Copy = new ConsoleKey[] { ConsoleKey.F5 };
      RenMov = new ConsoleKey[] { ConsoleKey.F6 };
      Mkdir = new ConsoleKey[] { ConsoleKey.F7 };
      Delete = new ConsoleKey[] { ConsoleKey.F8 };
      Touch = new ConsoleKey[] { ConsoleKey.F9 };
      Quit = new ConsoleKey[] { ConsoleKey.F10, ConsoleKey.Q };
      Up = new ConsoleKey[] { ConsoleKey.UpArrow, ConsoleKey.K };
      Down = new ConsoleKey[] { ConsoleKey.DownArrow, ConsoleKey.J };
      Left = new ConsoleKey[] { ConsoleKey.LeftArrow };
      Right = new ConsoleKey[] { ConsoleKey.RightArrow };
      PgUp = new ConsoleKey[] { ConsoleKey.PageUp };
      PgDown = new ConsoleKey[] { ConsoleKey.PageDown };
      Tab = new ConsoleKey[] { ConsoleKey.Tab };
      Backspace = new ConsoleKey[] { ConsoleKey.Backspace };
    }
  }

  public class FileViewKeys : KeySet {
    public FileViewKeys() {
      HelpLine = new string[] { "Help", "UnWrap", "Quit", "Hex", "Goto", null, "Search", "Raw", "Format", "Quit" };
      Help = new ConsoleKey[] { ConsoleKey.F1 };
      UnWrap = new ConsoleKey[] { ConsoleKey.F2 };
      Hex = new ConsoleKey[] { ConsoleKey.F4 };
      Goto = new ConsoleKey[] { ConsoleKey.F5 };
      Search = new ConsoleKey[] { ConsoleKey.F7 };
      Raw = new ConsoleKey[] { ConsoleKey.F8 };
      Format = new ConsoleKey[] { ConsoleKey.F9 };
      Quit = new ConsoleKey[] { ConsoleKey.F3, ConsoleKey.F10, ConsoleKey.Q };
      Up = new ConsoleKey[] { ConsoleKey.UpArrow, ConsoleKey.K };
      Down = new ConsoleKey[] { ConsoleKey.DownArrow, ConsoleKey.J };
      PgUp = new ConsoleKey[] { ConsoleKey.PageUp };
      PgDown = new ConsoleKey[] { ConsoleKey.PageDown };
    }
  }

  public class FileEditorKeys : KeySet {
    public FileEditorKeys() {
      HelpLine = new string[] { "Help", "Save", "Mark", "Replac", "Copy", "Move", "Search", "Delete", null, "Quit" };
      Help = new ConsoleKey[] { ConsoleKey.F1 };
      Save = new ConsoleKey[] { ConsoleKey.F2 };
      Mark = new ConsoleKey[] { ConsoleKey.F3 };
      Replace = new ConsoleKey[] { ConsoleKey.F4 };
      Copy = new ConsoleKey[] { ConsoleKey.F5 };
      Move = new ConsoleKey[] { ConsoleKey.F6 };
      Search = new ConsoleKey[] { ConsoleKey.F7 };
      Delete = new ConsoleKey[] { ConsoleKey.F8, ConsoleKey.Delete };
      Quit = new ConsoleKey[] { ConsoleKey.F10 };
      Tab = new ConsoleKey[] { ConsoleKey.Tab };
    }
  }
}
