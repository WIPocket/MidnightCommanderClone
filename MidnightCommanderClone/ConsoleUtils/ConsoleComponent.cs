using System;

namespace MidnightCommanderClone {
  public abstract class ConsoleComponent {
    public ConsoleUnit TargetWidth;
    public Characters Width { get; protected set; }
    public ConsoleUnit TargetHeight;
    public Characters Height { get; protected set; }
    public ConsoleBuffer RenderBuffer;
    protected ConsoleBuffer Buffer {
      get {
        if(RenderBuffer == null) return Parent.Buffer;
        return RenderBuffer;
      }
    }

    public (int x, int y) Position;

    public ConsoleLayout Parent;

    public virtual bool Selected {
      get {
        if(Parent == null) return true;
        return Parent.SelectedChild == this;
      }
    }
    public bool ParentSelected {
      get {
        return Parent == null || (Parent.ParentSelected && Selected);
      }
    }

    public abstract void Render();

    public virtual void Resize(Characters width, Characters height) {
      this.Width = width;
      this.Height = height;
    }

    public virtual void HandleKeyPress(ConsoleKeyInfo info) {

    }

    public virtual T FindParent<T>() where T : ConsoleLayout {
      if(Parent == null) return (T) null;
      if(Parent is T) return (T) Parent;
      return Parent.FindParent<T>();
    }

    protected virtual void Place(int x, int y, ConsoleCharacter character) {
      Buffer.Buffer[x + Position.x, y + Position.y] = character;
    }

    protected virtual void ShowCursor(int x, int y) {
      Buffer.ShowCursor(x + Position.x, y + Position.y);
    }
  }
}
