using System;

namespace MidnightCommanderClone {
  public static class MidnightCommanderClone {
    public static ConsoleColor Foreground = ConsoleColor.White;
    public static ConsoleColor Background = ConsoleColor.DarkBlue;

    public static void Main(string[] argv) {
      //Background = ConsoleColor.Black;
      var ui = new ConsoleUI();
      ui.Interactive();
    }
  }
}
