using System;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class FilesystemListing : ConsoleLayout {
    public ConsoleComponent Header;

    public List<FileOrDirectory> Listing = new List<FileOrDirectory>();
    public int Selected = 0;
    public int Top = 0;

    public override void Render() {
      Header.Position = Position;
      Header.Resize(Width, Height);
      Header.Parent = this;
      Header.Render();

      var nameLength = Width - (1 + 7 + 1 + 12);
      for(int y = 0; y < Height - 1; y++) {
        FileOrDirectory file = null;
        bool exists = true;
        if(y + Top < Listing.Count) file = Listing[y + Top];
        else exists = false;

        var selected = Selected == y + Top;
        for(int x = 0; x < Width; x++) {
          ConsoleCharacter c;
          if(exists && x >= Width -12) c = new ConsoleCharacter(file.ModifyTimeFormatted[x - (Width -12)]);
          else if(x == Width -12 -1) c = new ConsoleCharacter('│', ConsoleColor.Gray, MidnightCommanderClone.Background);
          else if(exists && x >= Width -12 -1 -7) c = new ConsoleCharacter(file.SizeFormatted[x - (Width -12 -1 -7)]);
          else if(x == Width -12 -1 -7 -1) c = new ConsoleCharacter('│', ConsoleColor.Gray, MidnightCommanderClone.Background);
          else if(exists && x < file.DisplayName.Length) c = new ConsoleCharacter(file.DisplayName[x]);
          else c = new ConsoleCharacter(' ');

          if(exists && !file.IsDirectory) c.Foreground = ConsoleColor.Gray;

          if(selected && ParentSelected) {
            c.Foreground = ConsoleColor.Black;
            c.Background = ConsoleColor.DarkCyan;
          }

          Place(x, y + 1, c);
        }
      }
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Up)) Selected--;
      else if(Keys.Is(info, Keys.Down)) Selected++;
      else if(Keys.Is(info, Keys.PgUp)) {
        Selected -= Height - 1;
        Top -= Height - 1;
      }
      else if(Keys.Is(info, Keys.PgDown)) {
        Selected += Height - 1;
        Top += Height - 1;
      }
      else if(Keys.Is(info, Keys.View)) {
        var selected = Listing[Selected];
        if(selected.IsDirectory) {
          FilesystemView parent = (FilesystemView) Parent;
          var oldPath = parent.Path;
          try {
            parent.Path = selected.Path;
            parent.UpdateList();
            Selected = Listing.FindIndex(x => x.Path == oldPath);
            Top = Selected - Height / 2;
          } catch (Exception e) {
            parent.Path = oldPath;
            parent.UpdateList();
            throw new Exception("Unable to open directory", e);
          }
        } else {
          var switcher = FindParent<UISwitcher>();
          switcher.Switch<UIFileView, FileViewKeys>();
          ((UIFileView) switcher.Current).ViewFile(selected.Path);
        }
      }
      else if(Keys.Is(info, Keys.Edit)) {
        var selected = Listing[Selected];
        if(!selected.IsDirectory) {
          var switcher = FindParent<UISwitcher>();
          switcher.Switch<UIFileEditor, FileViewKeys>();
          ((UIFileEditor) switcher.Current).ViewFile(selected.Path);
        }
      }
      else base.HandleKeyPress(info);

      Selected = Math.Max(0, Math.Min(Selected, Listing.Count - 1));
      if(Selected < Top) Top -= Height / 2;
      if(Selected >= Top + Height - 1) Top += Height / 2;
      Top = Math.Max(0, Math.Min(Top, Listing.Count - Height + 1));
    }
  }

  public class FileOrDirectory {
    public string Path;
    public string DisplayName;
    public bool IsDirectory;
    public string SizeFormatted;
    public string ModifyTimeFormatted;
  }
}
