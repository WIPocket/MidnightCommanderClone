using System;

namespace MidnightCommanderClone {
  public class Span : ConsoleComponent {
    public ConsoleColor Foreground = MidnightCommanderClone.Foreground;
    public ConsoleColor Background = MidnightCommanderClone.Background;
    public ConsoleColor ForegroundSelected = ConsoleColor.Black;
    public ConsoleColor BackgroundSelected = ConsoleColor.DarkCyan;
    public char PadCharacter = ' ';
    public bool CenterText = false;

    public string Text = "";

    public override void Render() {
      var offset = CenterText ? Width / 2 - Text.Length / 2 : 0;
      for(int i = 0; i < Width; i++) {
        Place(i, 0, new ConsoleCharacter(
          i - offset < Text.Length && i - offset >= 0 ? Text[i - offset] : PadCharacter,
          ParentSelected ? ForegroundSelected : Foreground,
          ParentSelected ? BackgroundSelected : Background
        ));
      }
    }
  }

  public class WrappingSpan : Span {
    private int LineOffset = 0;
    private int MaxLine = 0;
    public bool AllowScroll = false;
    public override void Render() {
      var words = Text.Split(' ');
      if(!AllowScroll) LineOffset = 0;
      int y = -LineOffset;
      int x = 0;

      for(int i = 0; i < words.Length; i++) {
        var word = words[i] + ' ';
        if(word.Length > Width - x) {
          while(x < Width) Place(x++, y, new ConsoleCharacter(' ', Foreground, Background));
          x = 0;
          y++;
        }

        foreach(var character in word) {
          if(character == '\n') {
            while(x < Width) Place(x++, y, new ConsoleCharacter(' ', Foreground, Background));
            x = 0;
            y++;
            continue;
          }
          if(x >= Width) {
            x = 0;
            y++;
          }
          if(y >= Height) break;
          Place(x++, y, new ConsoleCharacter(character, Foreground, Background));
        }
      }

      while(y < Height) {
        while(x < Width) Place(x++, y, new ConsoleCharacter(' ', Foreground, Background));
        x = 0;
        y++;
      }
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Up))
        LineOffset--;
      if(Keys.Is(info, Keys.Down))
        LineOffset++;
      LineOffset = Math.Max(0, Math.Min(LineOffset, 999999999));
    }

    protected override void Place(int x, int y, ConsoleCharacter character) {
      if(x >= 0 && y >= 0)
        base.Place(x, y, character);
    }
  }
}
