using System;

namespace MidnightCommanderClone {
  public class HelpLine : ConsoleLayoutVertical {
    public HelpLine() {
      TargetHeight = (Characters) 1;
      for(int i = 0; i < 10; i++) {
        AddChild(new Span() { TargetWidth = (Characters) 2, Foreground = ConsoleColor.White, Background = ConsoleColor.Black });
        AddChild(new Span() { TargetWidth = (Ratio) 1, Foreground = ConsoleColor.Black, Background = ConsoleColor.DarkCyan });
      }
    }

    public void ShowHelp(string[] help) {
      for(int i = 0; i < 10; i++) {
        var numberSpan = (Span) Children[i * 2];
        var helpTextSpan = (Span) Children[i * 2 + 1];

        if(help[i] == null)
          numberSpan.Text = "  ";
        else
          numberSpan.Text = (i + 1).ToString().PadLeft(2, ' ');
        helpTextSpan.Text = help[i] == null ? "" : help[i];
      }
    }
  }
}
