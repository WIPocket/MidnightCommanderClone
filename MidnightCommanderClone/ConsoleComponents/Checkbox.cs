using System;

namespace MidnightCommanderClone {
  public class Checkbox : DialogElement {
    public string Title = "Checkbox";
    public bool Checked;
    public ConsoleColor Foreground = MidnightCommanderClone.Foreground;
    public ConsoleColor Background = MidnightCommanderClone.Background;
    public ConsoleColor BackgroundSelected = ConsoleColor.DarkCyan;

    public override void Render() {
      for(int i = 0; i < Width; i++) {
        ConsoleCharacter c = new ConsoleCharacter(' ', Foreground, Background);
        if(i == 0) c = new ConsoleCharacter('[', Foreground, Background);
        if(i == 1) c = new ConsoleCharacter(Checked ? 'x' : ' ', Foreground, Background);
        if(i == 2) c = new ConsoleCharacter(']', Foreground, Background);
        if(i == 3) c = new ConsoleCharacter(' ', Foreground, Background);
        if(i >= 4 && i - 4 < Title.Length) c = new ConsoleCharacter(Title[i - 4], Foreground, Background);
        Place(i, 0, c);
      }

      if(Selected) ShowCursor(1, 0);
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(info.Key == ConsoleKey.Spacebar)
        Checked ^= true;
    }
  }
}
