using System;

namespace MidnightCommanderClone {
  public class Button : DialogElement {
    public string Title = "Button";
    public ConsoleColor Foreground = MidnightCommanderClone.Foreground;
    public ConsoleColor Background = MidnightCommanderClone.Background;
    public ConsoleColor BackgroundSelected = ConsoleColor.DarkCyan;

    public override void Render() {
      var offset = Width / 2 - Title.Length / 2;

      for(int i = 0; i < Width; i++) {
        ConsoleCharacter c = new ConsoleCharacter(' ', Foreground, Background);
        if(i == 0) c = new ConsoleCharacter('[', Foreground, Background);
        if(i == 1) c = new ConsoleCharacter(DefaultAction ? '<' : ' ', Foreground, Background);
        if(i == Width - 2) c = new ConsoleCharacter(DefaultAction ? '>' : ' ', Foreground, Background);
        if(i == Width - 1) c = new ConsoleCharacter(']', Foreground, Background);
        if(i >= offset && i < Title.Length + offset) c = new ConsoleCharacter(Title[i - offset], Foreground, Background);
        if(Selected) c.Background = BackgroundSelected;
        Place(i, 0, c);
      }

      if(Selected) ShowCursor(offset, 0);
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(info.Key == ConsoleKey.Enter)
        Use();
    }
  }
}
