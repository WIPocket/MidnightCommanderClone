using System;
using System.Text;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class Input : DialogElement {
    public string Text {
      get {
        var sb = new StringBuilder(TextChars.Count);
        foreach(var c in TextChars)
          sb.Append(c);
        return sb.ToString();
      }
      set {
        TextChars = new LinkedList<char>(value);
        CharAtCursor = null;
      }
    }
    public LinkedList<char> TextChars = new LinkedList<char>();
    public int Offset = 0;
    public LinkedListNode<char> FirstNode;
    public LinkedListNode<LinkedList<char>> TextCharsNode;
    public int CursorPosition = 0;
    public int UserCursorPosition = 0;
    public LinkedListNode<char> CharAtCursor;
    public ConsoleColor ForegroundColor = ConsoleColor.Black;
    public ConsoleColor BackgroundColor = ConsoleColor.DarkCyan;
    public bool SelectionDisabled = false;
    public MultilineInput Ml = null;
    public int LineNumber = 0;

    public override void Render() {
      MoveCursor(CursorPosition);
      var character = FirstNode;
      for(int i = 0; i < Width; i++) {
        var cc =new ConsoleCharacter(character == null ? ' ' : character.Value, ForegroundColor, BackgroundColor);
        if(Ml != null && character != null) {
          var s = Ml.SelectionStart;
          var e = Ml.SelectionEnd;
          var x = Offset + i;
          var y = LineNumber;
          var selected = false;

          if(s.y < y && e.y > y) selected = true;
          if(s.y == y && e.y > y && s.x <= x) selected = true;
          if(s.y < y && e.y == y && e.x >= x) selected = true;
          if(s.y == y && e.y == y && s.x <= x && e.x >= x) selected = true;

          if(selected) {
            cc.Foreground = ConsoleColor.Black;
            cc.Background = ConsoleColor.DarkCyan;
          }
        }
        Place(i, 0, cc);
        character = character?.Next;
      }
      if(Selected) ShowCursor(CursorPosition - Offset, 0);
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(info.Key == ConsoleKey.LeftArrow)
        MoveCursor(CursorPosition - 1, true);
      else if(info.Key == ConsoleKey.RightArrow)
        MoveCursor(CursorPosition + 1, true);
      else if(info.Key == ConsoleKey.Backspace)
        RemoveChar();
      else if(info.Key == ConsoleKey.Delete) {
        if(CursorPosition < TextChars.Count) {
          MoveCursor(CursorPosition + 1, true);
          RemoveChar();
        }
      }
      else if(info.Key == ConsoleKey.Home)
        MoveCursor(0, true);
      else if (info.Key == ConsoleKey.End)
        MoveCursor(TextChars.Count, true);
      else if(info.Key == ConsoleKey.Enter)
        if(Ml == null) FindParent<ConsoleLayoutDialog>().DoDefaultAction();
        else Ml.CreateNewline();
      else {
        WriteChar(info.KeyChar);
      }
    }

    public void MoveCursor(int NewPosition, bool userSet = false, bool force = false) {
      if(!force && !Selected && Offset != 0) return;
      if(CharAtCursor == null) {
        CursorPosition = 0;
        CharAtCursor = TextChars.First;
      }

      if(FirstNode == null && Offset > TextChars.Count) {
        Offset = TextChars.Count;
        FirstNode = TextChars.Last;
      }

      if(FirstNode == null || FirstNode.List == null) FirstNode = CharAtCursor;

      var oldPosition = CursorPosition;
      CursorPosition = NewPosition;
      if(userSet && CursorPosition < 0 && Ml != null) Ml.MoveCursor(-1);
      if(userSet && CursorPosition > TextChars.Count && Ml != null) Ml.MoveCursor(1);
      CursorPosition = Math.Max(0, Math.Min(CursorPosition, TextChars.Count));

      if(userSet) UserCursorPosition = CursorPosition;

      var diff = oldPosition - CursorPosition;
      if(TextChars.Count == 0) return;
      while(diff > 0) {
        diff--;
        CharAtCursor = CharAtCursor.Previous;
      }
      while(diff < 0) {
        diff++;
        CharAtCursor = CharAtCursor.Next;
      }

      while(Offset > CursorPosition) {
        Offset--;
        FirstNode = FirstNode.Previous;
      }
      while(Offset < CursorPosition - Width + 1) {
        Offset++;
        FirstNode = FirstNode.Next;
      }
    }

    public void SetOffset(int target) {
      if(Offset > TextChars.Count) Offset = target;
      while(Offset > target) {
        Offset--;
        if(Offset == TextChars.Count - 1)
          FirstNode = TextChars.Last;
        else
          FirstNode = FirstNode?.Previous;
      }
      while(Offset < target) {
        Offset++;
        FirstNode = FirstNode?.Next;
        if(FirstNode == null)
          Offset = target;
      }
    }

    protected void RemoveChar() {
      if(CursorPosition == 0) {
        if(Ml != null)
          Ml.RemoveNewline();
        return;
      }
      if(CursorPosition == TextChars.Count) {
        TextChars.RemoveLast();
        if(TextChars.Count >= Width - 1) {
          Offset--;
          FirstNode = FirstNode.Previous;
        }
      }
      else
        TextChars.Remove(CharAtCursor.Previous);
      CursorPosition--;
      UserCursorPosition = CursorPosition;
      if(Ml != null) Ml.EditorInfo.Modified = true;
      if(Ml != null) Ml.MoveSelection(-1, 0);
    }

    public void WriteChar(char character) {
      if(CursorPosition == TextChars.Count)
        TextChars.AddLast(character);
      else {
        TextChars.AddBefore(CharAtCursor, character);
        if(CursorPosition == Offset)
          FirstNode = CharAtCursor.Previous;
      }
      CursorPosition++;
      MoveCursor(CursorPosition, true);
      if(Ml != null) Ml.EditorInfo.Modified = true;
      if(Ml != null) Ml.MoveSelection(1, 0);
    }
  }
}
