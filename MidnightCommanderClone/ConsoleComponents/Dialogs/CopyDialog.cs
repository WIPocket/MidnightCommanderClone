using System;
using System.IO;

namespace MidnightCommanderClone {
  public class CopyDialog : FileOperationDialog {
    public override string Title { get { return " Copy "; } }

    public CopyDialog() : base() { }

    public override void DoFileOperation(string source, string destination) {
      if(destination.StartsWith(source))
        throw new Exception("Can not copy a folder into it self! (I mean it's possible, but you really don't want to do that. Trust me. I tried.)");
      if(File.GetAttributes(source).HasFlag(FileAttributes.Directory)) {
        foreach (DirectoryInfo dir in new DirectoryInfo(source).GetDirectories()) {
          string newDirPath = Path.Combine(destination, dir.Name);
          Directory.CreateDirectory(newDirPath);
          DoFileOperation(dir.FullName, newDirPath);
        }
        foreach (string newPath in Directory.GetFiles(source)) {
          File.Copy(newPath, destination, true);
        }
      } else
        File.Copy(source, destination, true);
    }

    public override void Show() {
      base.Show();
      TargetPath.Text = Path.Combine(TargetPath.Text, Path.GetFileName(SourcePath));
    }
  }
}
