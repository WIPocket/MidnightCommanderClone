using System;

namespace MidnightCommanderClone {
  public class ConfirmReplaceDialog : FileOperationDialog {
    public override string Title { get { return "Replace selection?"; } }
    public override Characters OuterHeight { get { return 8; } }
    public string Search, Replacement;
    public MultilineInput Editor;

    public ConfirmReplaceDialog() {
      Children.Clear();
      AddChild(new Span() { Text = "", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
      AddChild(new Span() { Text = "", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });

      AddChild(new Padding() { TargetHeight = 1 });

      var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      buttons.AddChild(new Button() {
        Title = "All",
        DefaultAction = false,
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          Editor.ReplaceString(Editor.SelectionStartChar, Editor.SelectionEndChar, Replacement);
          Editor.ReplaceString(Search, Replacement, true);
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
            view.UpdateList();
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Button() {
        Title = "Replace",
        DefaultAction = true,
        DefaultSelect = true,
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          Editor.ReplaceString(Editor.SelectionStartChar, Editor.SelectionEndChar, Replacement);
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
            view.UpdateList();
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
          Editor.ReplaceString(Search, Replacement);
        }
      });
      buttons.AddChild(new Button() {
        Title = "Skip",
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
            view.UpdateList();
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
          Editor.ReplaceString(Search, Replacement);
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = 2 });
      buttons.AddChild(new Button() {
        Title = "Cancel",
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      AddChild(buttons);
    }

    public override void Show() {
      ((Span) Children[0]).Text = "Search: " + Search;
      ((Span) Children[0]).Text = "Replace: " + Replacement;
      FindParent<ConsoleLayoutOverlay>().Enabled = true;
    }
    public override void DoFileOperation(string source, string destination) {

    }
  }
}
