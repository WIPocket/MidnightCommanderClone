using System;
using System.IO;

namespace MidnightCommanderClone {
  public class MkDirDialog : FileOperationDialog {
    public override string Title { get { return " Make directory "; } }

    public MkDirDialog() : base() {
      ((Span) Child).Text = "Name:";
    }

    public override void DoFileOperation(string source, string destination) {
      Directory.CreateDirectory(Path.Combine(source, destination));
    }

    public override void Show() {
      base.Show(true);
      TargetPath.Text = "";
    }
  }
}
