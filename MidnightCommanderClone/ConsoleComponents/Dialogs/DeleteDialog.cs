using System;
using System.Collections.Generic;
using System.IO;

namespace MidnightCommanderClone {
  public class DeleteDialog : FileOperationDialog {
    public override string Title { get { return " Delete "; } }
    public Span Path;

    public DeleteDialog() {
      Children = new List<ConsoleComponent>();
      AddChild(new Span() { Text = "Are you sure you want to delete this?", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Red });
      AddChild(Path = new Span() { Text = "[]", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });

      AddChild(new Padding() { TargetHeight = 1 });

      var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      buttons.AddChild(OKButton = new Button() {
        Title = "OK",
        TargetWidth = 10,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          DoFileOperation(SourcePath, "");
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
          view.UpdateList();
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = 2 });
      buttons.AddChild(new Button() {
        Title = "Cancel",
        DefaultSelect = true,
        TargetWidth = 10,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      AddChild(buttons);
    }

    public override void DoFileOperation(string target, string _) {
      if(File.GetAttributes(target).HasFlag(FileAttributes.Directory))
        new DirectoryInfo(target).Delete(true);
      else
        new FileInfo(target).Delete();
    }

    public override void Show() {
      var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
      var from = fsViews.Find(view => view.Selected);
      SourcePath = from.FilesystemList.Listing[from.FilesystemList.Selected].Path;
      Path.Text = "\"" + SourcePath + "\"";
      FindParent<ConsoleLayoutOverlay>().Enabled = true;
    }
  }
}
