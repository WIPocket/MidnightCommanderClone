using System;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class ConfirmOperationBox : Box {
    public ConfirmOperationBox() {
      PaddingVertical = 1;
    }

    public override void Render() {
      base.Render();
      HLine(3);
    }
  }

  public class ConfirmSavePopup : ConsoleLayoutPopup {
    ConfirmOperationBox box;
    ConsoleLayoutDialog dialog;

    public ConfirmSavePopup() {
      TargetHeight = 8;
      TargetWidth = 80;
      Background = ConsoleColor.Gray;
      AddChild(box = new ConfirmOperationBox() { Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black, CenterTitle = true });
      box.AddChild(dialog = new ConsoleLayoutDialog());
      Resize((Characters) TargetWidth, (Characters) TargetHeight);
    }

    public void ConfirmSave(string path, string content, bool isExitting) {
      var overlay = FindParent<ConsoleLayoutOverlay>();
      overlay.Enabled = true;
      dialog.Children = new List<ConsoleComponent>();
      dialog.Elements = null;
      if(!isExitting) {
        box.Title = " Save file ";
        dialog.AddChild(new Span() { Text = "Confirm save file:", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
        dialog.AddChild(new Span() { Text = path, TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
        dialog.AddChild(new Padding() { TargetHeight = 1 });
        var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
        dialog.AddChild(buttons);
        buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
        buttons.AddChild(new Button() {
          Title = "Save",
          TargetWidth = 10,
          Foreground = ConsoleColor.Black,
          Background = ConsoleColor.Gray,
          DefaultSelect = true,
          Use = () => {
            System.IO.File.WriteAllText(path, content);
            overlay.Enabled = false;
            FindParent<UIContainer>().FindChildren<EditorInfoSpan>()[0].Modified = false;
          }
        });
        buttons.AddChild(new Button() {
          Title = "Cancel",
          TargetWidth = 10,
          Foreground = ConsoleColor.Black,
          Background = ConsoleColor.Gray,
          Use = () => {
            overlay.Enabled = false;
          }
        });
        buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      } else {
        box.Title = " Close file ";
        dialog.AddChild(new Span() { Text = "File " + path + " was modified.", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
        dialog.AddChild(new Span() { Text = "Save before close?", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
        dialog.AddChild(new Padding() { TargetHeight = 1 });
        var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
        dialog.AddChild(buttons);
        buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
        buttons.AddChild(new Button() {
          Title = "Yes",
          TargetWidth = 10,
          Foreground = ConsoleColor.Black,
          Background = ConsoleColor.Gray,
          Use = () => {
            System.IO.File.WriteAllText(path, content);
            overlay.Enabled = false;
            FindParent<UIContainer>().Switcher.Switch<UIFileExplorer, FileExplorerKeys>();
          }
        });
        buttons.AddChild(new Button() {
          Title = "No",
          TargetWidth = 10,
          Foreground = ConsoleColor.Black,
          Background = ConsoleColor.Gray,
          Use = () => {
            overlay.Enabled = false;
            FindParent<UIContainer>().Switcher.Switch<UIFileExplorer, FileExplorerKeys>();
          }
        });
        buttons.AddChild(new Button() {
          Title = "Cancel",
          TargetWidth = 10,
          Foreground = ConsoleColor.Black,
          Background = ConsoleColor.Gray,
          DefaultSelect = true,
          Use = () => {
            overlay.Enabled = false;
          }
        });
        buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      }

      box.Resize(box.Width, box.Height);
    }
  }
}
