using System;
using System.IO;

namespace MidnightCommanderClone {
  public abstract class FileOperationDialog : ConsoleLayoutDialog {
    public Input TargetPath;
    public string SourcePath;
    public Button OKButton;
    public abstract string Title { get; }
    public virtual Characters OuterHeight { get { return 8; } }

    public FileOperationDialog() {
      AddChild(new Span() { Text = "to:", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
      AddChild(TargetPath = new Input() { TargetHeight = 1, DefaultSelect = true, Text = "Hello, world!" });
      AddChild(new Padding() { TargetHeight = 1 });

      var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      buttons.AddChild(OKButton = new Button() {
        Title = "OK",
        DefaultAction = true,
        TargetWidth = 10,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          DoFileOperation(SourcePath, TargetPath.Text);
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
            view.UpdateList();
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = 2 });
      buttons.AddChild(new Button() {
        Title = "Cancel",
        TargetWidth = 10,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      AddChild(buttons);
    }

    public virtual void Show() {
      Show(false);
    }

    public void Show(bool sourcePathHere) {
      var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
      var from = fsViews.Find(view => view.Selected);
      var to = fsViews.Find(view => !view.Selected);
      SourcePath = from.FilesystemList.Listing[from.FilesystemList.Selected].Path;
      if(sourcePathHere) SourcePath = from.Path;
      TargetPath.Text = to.Path;
      SelectedElement = 0;
      FindParent<ConsoleLayoutOverlay>().Enabled = true;
    }

    public abstract void DoFileOperation(string source, string destination);

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(info.Key == ConsoleKey.F10)
        FindParent<ConsoleLayoutOverlay>().Enabled = false;
      else base.HandleKeyPress(info);
    }
  }

  public class FileOperationBox : Box {
    public FileOperationBox() {
      PaddingVertical = 1;
    }

    public override void Render() {
      base.Render();
      HLine(Height - 3);
    }
  }
}
