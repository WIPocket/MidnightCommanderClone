using System;
using System.IO;

namespace MidnightCommanderClone {
  public class TouchDialog : FileOperationDialog {
    public override string Title { get { return " Make file "; } }

    public TouchDialog() : base() {
      ((Span) Child).Text = "Name:";
    }

    public override void DoFileOperation(string source, string destination) {
      File.Create(Path.Combine(source, destination)).Close();
    }

    public override void Show() {
      base.Show(true);
      TargetPath.Text = "";
    }
  }
}
