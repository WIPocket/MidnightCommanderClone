using System;

namespace MidnightCommanderClone {
  public class ReplaceStringDialog : FileOperationDialog {
    public Input FindStringInput;
    public Input ReplaceStringInput;
    public MultilineInput Editor;

    public override string Title { get { return " Find and replace "; } }
    public override Characters OuterHeight { get { return 10; } }

    public ReplaceStringDialog() {
      Children.Clear();
      AddChild(new Span() { Text = "Search:", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
      AddChild(FindStringInput = new Input() { TargetHeight = 1, DefaultSelect = true, Text = "" });
      AddChild(new Span() { Text = "Replace:", TargetHeight = 1, Background = ConsoleColor.Gray, Foreground = ConsoleColor.Black });
      AddChild(ReplaceStringInput = new Input() { TargetHeight = 1, Text = "" });

      AddChild(new Padding() { TargetHeight = 1 });

      var buttons = new ConsoleLayoutVertical() { TargetHeight = 1 };
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      buttons.AddChild(new Button() {
        Title = "Replace",
        DefaultAction = true,
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
          Editor.ReplaceString(FindStringInput.Text, ReplaceStringInput.Text);
          var fsViews = FindParent<UIContainer>().FindChildren<FilesystemView>();
          foreach(var view in fsViews)
            view.UpdateList();
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = 2 });
      buttons.AddChild(new Button() {
        Title = "Cancel",
        TargetWidth = 14,
        Foreground = ConsoleColor.Black,
        Background = ConsoleColor.Gray,
        Use = () => {
          FindParent<ConsoleLayoutOverlay>().Enabled = false;
        }
      });
      buttons.AddChild(new Padding() { TargetWidth = (Ratio) 1 });
      AddChild(buttons);
    }

    public override void Show() {
      FindParent<ConsoleLayoutOverlay>().Enabled = true;
    }

    public override void DoFileOperation(string src, string dst) {
      throw new NotImplementedException();
    }
  }
}
