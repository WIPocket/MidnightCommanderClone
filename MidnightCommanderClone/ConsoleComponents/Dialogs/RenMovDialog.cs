using System;
using System.IO;

namespace MidnightCommanderClone {
  public class RenMovDialog : FileOperationDialog {
    public override string Title { get { return " Rename or move "; } }

    public RenMovDialog() : base() { }

    public override void DoFileOperation(string source, string destination) {
      dynamic FSOInfo;
      if(File.GetAttributes(source).HasFlag(FileAttributes.Directory))
        FSOInfo = new DirectoryInfo(source);
      else
        FSOInfo = new FileInfo(source);
      FSOInfo.MoveTo(destination);
    }

    public override void Show() {
      base.Show();
      TargetPath.Text = SourcePath;
    }
  }
}
