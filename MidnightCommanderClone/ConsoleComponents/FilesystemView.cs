using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class FilesystemView : Box {
    public FilesystemListing FilesystemList = new FilesystemListing();
    public string Path = Directory.GetCurrentDirectory();
    private DirectoryInfo Dir;

    public FilesystemView() {
      TargetWidth = (Ratio) 1;
      AddChild(FilesystemList);
      UpdateList();
      var header = new ConsoleLayoutVertical();
      header.AddChild(new Span() { Text = "Name", CenterText = true, Foreground = ConsoleColor.Yellow, TargetWidth = (Ratio) 1 });
      header.AddChild(new CharacterComponent(new ConsoleCharacter('│', ConsoleColor.Gray, MidnightCommanderClone.Background)));
      header.AddChild(new Span() { Text = "Size", CenterText = true, Foreground = ConsoleColor.Yellow, TargetWidth = (Characters) 7 });
      header.AddChild(new CharacterComponent(new ConsoleCharacter('│', ConsoleColor.Gray, MidnightCommanderClone.Background)));
      header.AddChild(new Span() { Text = "Modify time", CenterText = false, Foreground = ConsoleColor.Yellow, TargetWidth = (Characters) 12 });
      FilesystemList.Header = header;
    }

    public void UpdateList() {
      FilesystemList.Listing.Clear();
      if(Path == "DRIVES") {
        Title = " Drives: ";
        var drives = DriveInfo.GetDrives();

        foreach(var drive in drives)
          FilesystemList.Listing.Add(new FileOrDirectory() {
            Path = drive.Name,
            DisplayName = " " + drive.Name,
            IsDirectory = true,
            SizeFormatted = "  <DIR>",
            ModifyTimeFormatted = drive.RootDirectory.LastWriteTimeUtc.ToString("MMM dd HH:mm")
          });

        return;
      }

      Dir = new DirectoryInfo(Path);
      Path = Dir.FullName; // Clean up ..
      SetTitle();

      var upDir = new FileOrDirectory() {
        Path = Dir.Parent?.FullName,
        DisplayName = "/..",
        IsDirectory = true,
        SizeFormatted = "UP--DIR",
        ModifyTimeFormatted = Dir.LastWriteTimeUtc.ToString("MMM dd HH:mm")
      };
      if (upDir.Path == null) upDir.Path = "DRIVES";
      FilesystemList.Listing.Add(upDir);

      var subdirs = Dir.GetDirectories();
      Array.Sort(subdirs, (a, b) => {
        for(int i = 0; i < Math.Min(a.Name.Length, b.Name.Length); i++) {
          if(a.Name[i] < b.Name[i]) return -1;
          if(a.Name[i] > b.Name[i]) return 1;
        }
        if(a.Name.Length > b.Name.Length) return 1;
        return -1;
      });
      foreach (DirectoryInfo item in subdirs) {
        FilesystemList.Listing.Add(new FileOrDirectory() {
          Path = item.FullName,
          DisplayName = "/" + item.Name,
          IsDirectory = true,
          SizeFormatted = "  <DIR>",
          ModifyTimeFormatted = item.LastWriteTimeUtc.ToString("MMM dd HH:mm")
        });
      }

      var files = Dir.GetFiles();
      Array.Sort(files, (a, b) => {
        for(int i = 0; i < Math.Min(a.Name.Length, b.Name.Length); i++) {
          if(a.Name[i] < b.Name[i]) return -1;
          if(a.Name[i] > b.Name[i]) return 1;
        }
        if(a.Name.Length > b.Name.Length) return 1;
        return -1;
      });
      foreach (FileInfo item in files) {
        FilesystemList.Listing.Add(new FileOrDirectory() {
          Path = item.FullName,
          DisplayName = " " + item.Name,
          IsDirectory = false,
          SizeFormatted = item.Length.ToString().PadLeft(7, ' '),
          ModifyTimeFormatted = item.LastWriteTimeUtc.ToString("MMM dd HH:mm")
        });
      }
    }

    public override void Render() {
      FilesystemList.Selected = Math.Max(0, Math.Min(FilesystemList.Selected, FilesystemList.Listing.Count - 1));
      base.Render();
      HLine(Height - 3);

      string displayName = FilesystemList.Listing[FilesystemList.Selected].DisplayName;
      if(displayName == "/..") displayName = "UP--DIR";
      for(int x = 1; x < Width - 1; x++)
        Place(x, Height - 2, new ConsoleCharacter(
          x - 1 < displayName.Length ? displayName[x - 1] : ' ',
          ConsoleColor.Gray,
          MidnightCommanderClone.Background
        ));

      DriveInfo drive = new DriveInfo(Dir.Root.FullName);
      string driveFullness = $" {HumanReadableFileSize(drive.AvailableFreeSpace)}/{HumanReadableFileSize(drive.TotalSize)} ({drive.AvailableFreeSpace * 100 / drive.TotalSize}%) ";
      for(int x = Width - 2 - driveFullness.Length; x < Width - 2; x++)
        Place(x, Height - 1, new ConsoleCharacter(
          driveFullness[x - (Width - 2 - driveFullness.Length)],
          ConsoleColor.Gray,
          MidnightCommanderClone.Background
        ));
    }

    private string HumanReadableFileSize(long size) {
      string[] sizeNames = { "B", "K", "M", "G", "T", "P" };
      int i = 0;
      while (size >= 9999 && i < sizeNames.Length - 1) {
        i++;
        size = size/1024;
      }
      return size + sizeNames[i];
    }

    public override void Resize(Characters width, Characters height) {
      base.Resize(width, height);
      Child.Resize(Child.Width, Child.Height - 2);
      SetTitle();
    }

    void SetTitle() {
      Title = $" {Path} ";
      if(Width != null && Path.Length - Width >= -8)
        Title = $" ...{Path.Remove(0, Path.Length - Width + 10)} ";
    }
  }
}
