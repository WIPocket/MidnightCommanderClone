using System;

namespace MidnightCommanderClone {
  public class Box : ConsoleLayout {
    public ConsoleColor Foreground = ConsoleColor.Gray;
    public ConsoleColor Background = MidnightCommanderClone.Background;
    public ConsoleColor ForegroundSelected = ConsoleColor.Black;
    public ConsoleColor BackgroundSelected = ConsoleColor.Gray;
    public char[] Border = new char[] { '┌', '─', '┐', '│', '┘', '─', '└', '│', '├', '┤' };
    public bool CenterTitle = false;
    public int PaddingVertical = 0;
    public int PaddingHorizontal = 0;

    public string Title = "";

    public override void Render() {
      Place(0, 0, new ConsoleCharacter(Border[0], Foreground, Background));

      var titleOffset = CenterTitle ? Width / 2 - Title.Length / 2 - 2 : 0;
      for(int i = 1; i < Width - 1; i++) Place(i, 0, new ConsoleCharacter(
        i > 1 + titleOffset && i < Title.Length + 2 + titleOffset ? Title[i - 2 - titleOffset] : Border[1],
        i > 1 + titleOffset && i < Title.Length + 2 + titleOffset && ParentSelected ? ForegroundSelected : Foreground,
        i > 1 + titleOffset && i < Title.Length + 2 + titleOffset && ParentSelected ? BackgroundSelected : Background
      ));
      Place(Width - 1, 0, new ConsoleCharacter(Border[2], Foreground, Background));
      for(int i = 1; i < Height - 1; i++) Place(Width - 1, i, new ConsoleCharacter(Border[3], Foreground, Background));
      Place(Width - 1, Height - 1, new ConsoleCharacter(Border[4], Foreground, Background));
      for(int i = 1; i < Width - 1; i++) Place(i, Height - 1, new ConsoleCharacter(Border[5], Foreground, Background));
      Place(0, Height - 1, new ConsoleCharacter(Border[6], Foreground, Background));
      for(int i = 1; i < Height - 1; i++) Place(0, i, new ConsoleCharacter(Border[7], Foreground, Background));

      Child.Render();
    }

    public override void Resize(Characters width, Characters height) {
      base.Resize(width, height);
      Child.Position = (Position.x + (1 + PaddingVertical), Position.y + (1 + PaddingHorizontal));
      Child.Resize(width - 2 * (1 + PaddingVertical), height - 2 * (1 + PaddingHorizontal));
    }

    protected void HLine(int y) {
      Place(0, y, new ConsoleCharacter(Border[8], Foreground, Background));
      for(int x = 1; x < Width - 1; x++)
        Place(x, y, new ConsoleCharacter(Border[1], Foreground, Background));
      Place(Width - 1, y, new ConsoleCharacter(Border[9], Foreground, Background));
    }
  }
}
