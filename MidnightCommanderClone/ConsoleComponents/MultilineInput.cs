using System;
using System.Text;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class MultilineInput : ConsoleLayoutHorizontal {
    public EditorInfoSpan EditorInfo;
    public bool SelectionMode = false;
    public ((int x, int y) p0, (int x, int y) p1) Selection = ((-1, -1), (-1, -1));

    public bool SelectionReversed {
      get {
        if(Selection.p0.y < Selection.p1.y) return false;
        if(Selection.p0.y > Selection.p1.y) return true;
        if(Selection.p0.x < Selection.p1.x) return false;
        return true;
      }
    }

    public bool HasSelection { get { return Selection.p0.x > -1 && Selection.p1.x > -1; } }

    public (int x, int y) SelectionStart { get { return SelectionReversed ? Selection.p1 : Selection.p0; } }

    public (int x, int y) SelectionEnd { get { return SelectionReversed ? Selection.p0 : Selection.p1; } }

    public (LinkedListNode<LinkedList<char>> p0, LinkedListNode<LinkedList<char>> p1) SelectionLine;
    public (LinkedListNode<char> p0, LinkedListNode<char> p1) SelectionChar;

    public LinkedListNode<LinkedList<char>> SelectionStartLine { get { return SelectionReversed ? SelectionLine.p1 : SelectionLine.p0; } }
    public LinkedListNode<LinkedList<char>> SelectionEndLine { get { return SelectionReversed ? SelectionLine.p0 : SelectionLine.p1; } }

    public LinkedListNode<char> SelectionStartChar { get { return SelectionReversed ? SelectionChar.p1 : SelectionChar.p0; } }
    public LinkedListNode<char> SelectionEndChar { get { return SelectionReversed ? SelectionChar.p0 : SelectionChar.p1; } }

    public string Text {
      get {
        var sb = new StringBuilder();
        foreach (var line in Lines) {
          sb.AppendLine(String.Join("", line));
        }
        return sb.ToString();
      }
      set {
        Lines = new LinkedList<LinkedList<char>>();
        foreach(string line in value.Split('\n'))
          Lines.AddLast(new LinkedList<char>(line));
        Children = new List<ConsoleComponent>();
        Top = Lines.First;
        SelectedChildIndex = 0;
        EditorInfo.Lines = Lines.Count;
        Resize(Width, Height);
      }
    }
    public LinkedListNode<LinkedList<char>> Top;
    public LinkedList<LinkedList<char>> Lines;
    public override void Resize(Characters width, Characters height) {
      while(Children.Count - 1 > height)
        Children.RemoveAt(Children.Count - 1);
      while(Children.Count - 1 < height)
        AddChild(new Input() { TargetHeight = 1, BackgroundColor = MidnightCommanderClone.Background, ForegroundColor = ConsoleColor.White, Ml = this });
      base.Resize(width, height);
      var ptr = Top;
      int off = 0;
      foreach(Input input in Children) {
        input.SelectionDisabled = ptr == null;
        input.LineNumber = EditorInfo.TopOffset + off++;
        if(ptr != null) {
          if(input.TextChars != ptr.Value) {
            input.TextChars = ptr.Value;
            input.TextCharsNode = ptr;
            input.CursorPosition = 0;
            input.CharAtCursor = null;
            input.FirstNode = null;
          }
          ptr = ptr.Next;
        } else {
          input.TextChars = new LinkedList<char>();
          input.CursorPosition = 0;
          input.CharAtCursor = null;
          input.FirstNode = null;
        }
      }

      EditorInfo.Recalc();
    }

    public override void Render() {
      base.Render();
    }

    private bool CorrectCursorPosition = true;

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      Input oldSelected = (Input) SelectedChild;

      if(info.Key == ConsoleKey.UpArrow)
        SelectedChildIndex--;
      else if(info.Key == ConsoleKey.DownArrow)
        SelectedChildIndex++;
      else if(info.Key == ConsoleKey.PageUp)
        SelectedChildIndex -= Height - 1;
      else if(info.Key == ConsoleKey.PageDown)
        SelectedChildIndex += Height - 1;
      else if(info.Key == ConsoleKey.F3) {
        SelectionMode ^= true;
        if(SelectionMode) {
          Selection = ((oldSelected.CursorPosition, EditorInfo.TopLine + EditorInfo.TopOffset), (-1, -1));
          SelectionLine.p0 = oldSelected.TextCharsNode;
          SelectionChar.p0 = oldSelected.CharAtCursor;
        }
      } else if(info.Key == ConsoleKey.F8)
        DeleteSelection();
      else if(info.Key == ConsoleKey.F7)
        FindParent<UIContainer>().FileOperation.Show<FindStringDialog>().Editor = this;
      else if(info.Key == ConsoleKey.F4)
        FindParent<UIContainer>().FileOperation.Show<ReplaceStringDialog>().Editor = this;
      else if(info.Key == ConsoleKey.F5)
        CopySelection();
      else if(info.Key == ConsoleKey.F6) {
        var paste = GetPaste();
        DeleteSelection();
        oldSelected.MoveCursor(oldSelected.CursorPosition, false, true);
        Selection = ((oldSelected.CursorPosition, EditorInfo.TopLine + EditorInfo.TopOffset), (-1, -1));
        SelectionLine.p0 = oldSelected.TextCharsNode;
        SelectionChar.p0 = oldSelected.CharAtCursor;
        CopySelection(paste);
        oldSelected.MoveCursor(oldSelected.CursorPosition, false, true);
        Selection.p1 = (oldSelected.CursorPosition, EditorInfo.TopLine + EditorInfo.TopOffset);
        Input ns = (Input) SelectedChild;
        SelectionLine.p1 = ns.TextCharsNode;
        SelectionChar.p1 = ns.CharAtCursor;
      }
      else if(Keys.Is(info, Keys.Quit))
        if(EditorInfo.Modified)
          FindParent<UIContainer>().ConfirmSave.ConfirmSave(EditorInfo.Path, Text, true);
        else
          FindParent<UISwitcher>().Switch<UIFileExplorer, FileExplorerKeys>();
      else SelectedChild.HandleKeyPress(info);



      Input newSelected = (Input) SelectedChild;

      if(CorrectCursorPosition) {
        newSelected.MoveCursor(oldSelected.UserCursorPosition);
        newSelected.UserCursorPosition = oldSelected.UserCursorPosition;
      }
      CorrectCursorPosition = true;

      foreach(Input input in Children) {
        input.SetOffset(newSelected.Offset);
      }

      EditorInfo.Column = newSelected.CursorPosition;
      EditorInfo.TopLine = SelectedChildIndex;
      EditorInfo.Lines = Lines.Count;
      EditorInfo.Character = newSelected.CharAtCursor?.Value;
      EditorInfo.Recalc();

      if(SelectionMode) {
        Selection.p1 = (oldSelected.CursorPosition, EditorInfo.TopLine + EditorInfo.TopOffset);
        SelectionLine.p1 = newSelected.TextCharsNode;
        SelectionChar.p1 = newSelected.CharAtCursor;
      }
    }

    public void DeleteSelection() {
      if(!HasSelection) return;
      var line = SelectionStartLine;
      if(line.Next != null) line = line.Next;
      var endLine = SelectionEndLine;

      if(SelectionStartLine != SelectionEndLine)
        while(line != endLine) {
          var d = line;
          line = line.Next;
          Lines.Remove(d);
        }

      var character = SelectionStartChar;
      var endChar = SelectionEndChar;
      while(character != null) {
        var d = character;
        character = character.Next;
        SelectionStartLine.Value.Remove(d);
        if(d == endChar) break;
      }
      if(SelectionStartLine != SelectionEndLine) {
        character = SelectionEndLine.Value.First;
        while(character != null) {
          var d = character;
          character = character.Next;
          SelectionEndLine.Value.Remove(d);
          if(d == endChar) break;
        }

        character = SelectionEndLine.Value.First;
        while(character != null) {
          SelectionStartLine.Value.AddLast(character.Value);
          character = character.Next;
        }
        Lines.Remove(SelectionEndLine);
      }

      Selection = ((-1, -1), (-1, -1));
      Children.Clear();
      Resize(Width, Height);
      EditorInfo.Modified = true;
    }

    public List<char> GetPaste() {
      var character = SelectionStartChar;
      var line = SelectionStartLine;
      var endChar = SelectionEndChar;
      var paste = new List<char>();
      while(character != endChar) {
        paste.Add(character.Value);
        character = character.Next;
        if(character == null) {
          if(line == SelectionEndLine) break;
          line = line.Next;
          character = line.Value.First;
          paste.Add('\n');
        }
      }
      if(character != null)
        paste.Add(character.Value);
      return paste;
    }

    public void CopySelection(List<char> paste = null) {
      if(paste == null) paste = GetPaste();

      foreach(var pasteChar in paste) {
        if(pasteChar == '\n')
          CreateNewline();
        else
          ((Input) SelectedChild).WriteChar(pasteChar);
      }
    }

    public void CreateNewline() {
      var sel = (Input) SelectedChild;
      var nextList = new LinkedList<char>();
      var c = sel.CharAtCursor;
      while(c != null) {
        var oc = c;
        c = c.Next;
        sel.TextChars.Remove(oc);
        nextList.AddLast(oc.Value);
      }
      Lines.AddAfter(sel.TextCharsNode, nextList);
      sel.CharAtCursor = null;
      Resize(Width, Height);
      MoveCursor(1);
      EditorInfo.Modified = true;
      MoveSelection(0, 1);
    }

    public void RemoveNewline() {
      var sel = (Input) SelectedChild;
      var oldc = sel.CharAtCursor;
      Lines.Remove(sel.TextCharsNode);
      Resize(Width, Height);
      MoveCursor(-1);
      var newSelected = (Input) SelectedChild;
      var c = oldc;
      while(c != null) {
        newSelected.TextChars.AddLast(c.Value);
        c = c.Next;
      }
      EditorInfo.Modified = true;
      MoveSelection(0, -1);
    }

    public void MoveSelection(int xOffset, int yOffset) {
      var start = SelectionStart;
      var end = SelectionEnd;
      var x = EditorInfo.Column;
      var y = EditorInfo.TopOffset + EditorInfo.TopLine;

      if(IsBefore(x, y, end.x, end.y)) {
        if(y == end.y) end.x += xOffset;
        end.y += yOffset;
      }

      if(IsBefore(x, y, start.x, start.y)) {
        if(y == start.y) start.x += xOffset;
        start.y += yOffset;
      }

      Selection = (start, end);
    }

    public bool IsBefore(int x1, int y1, int x2, int y2) {
      return (y1 < y2) || (y1 == y2 && x1 < x2);
    }

    public void MoveCursor(int ldiff) {
      if(ldiff > 1) {
        for(int i = 0; i < ldiff; i++)
          MoveCursor(1);
        return;
      }
      if(ldiff < -1) {
        for(int i = 0; i < -ldiff; i++)
          MoveCursor(-1);
        return;
      }

      SelectedChildIndex += ldiff;
      if(SelectedChildIndex < 0 || SelectedChildIndex >= Lines.Count) {
        SelectedChildIndex -= ldiff;
        return;
      }

      ScrollToFitCursor();
      var sel = (Input) SelectedChild;
      if(ldiff == -1) sel.MoveCursor(sel.TextChars.Count, true);
      else sel.MoveCursor(0, true);
      CorrectCursorPosition = false;
    }

    protected void ScrollToFitCursor() {
      if(SelectedChildIndex < 0) {
        while(SelectedChildIndex < 0) {
          SelectedChildIndex++;
          if(Top.Previous != null) {
            Top = Top.Previous;
            EditorInfo.TopOffset--;
          }
        }
        Resize(Width, Height);
      }
      if(SelectedChildIndex > Height - 1) {
        while(SelectedChildIndex > Height - 1) {
          SelectedChildIndex--;
          if(Top.Next != null) {
            Top = Top.Next;
            EditorInfo.TopOffset++;
          }
        }
        Resize(Width, Height);
      }
      if(((Input) SelectedChild).SelectionDisabled) SelectedChildIndex--;
    }

    public void FindString(string search) {
      Selection = ((-1, -1), (-1, -1));
      SelectionChar = (null, null);
      SelectionLine = (null, null);
      var sel = (Input) SelectedChild;
      var character = sel.CharAtCursor;
      var line = sel.TextCharsNode;
      (int x, int y) cpos = (sel.CursorPosition, EditorInfo.TopLine + EditorInfo.TopOffset);
      while(true) {
        if(character == null) {
          line = line.Next;
          if(line == null) break;
          character = line.Value.First;
          cpos.x = 0;
          cpos.y++;
          if(character == null) continue;
        }
        var end = FindEnd(character, search);
        if(end == null) {
          character = character.Next;
          cpos.x++;
        } else {
          Selection = ((cpos.x, cpos.y), (cpos.x + search.Length - 1, cpos.y));
          SelectionChar = (character, end);
          SelectionLine = (line, line);
          MoveCursor(cpos.y - EditorInfo.TopLine + EditorInfo.TopOffset);
          MoveCursor(5);
          MoveCursor(-5);
          break;
        }
      }
    }

    public void ReplaceString(string search, string replacement, bool all = false) {
      FindString(search);
      if(HasSelection) {
        if(all) {
          ReplaceString(SelectionStartChar, SelectionEndChar, replacement);
          ReplaceString(search, replacement, all);
        } else {
          var dialog = FindParent<UIContainer>().FileOperation.Show<ConfirmReplaceDialog>();
          dialog.Editor = this;
          dialog.Search = search;
          dialog.Replacement = replacement;
        }
      }
    }

    public void ReplaceString(LinkedListNode<char> start, LinkedListNode<char> end, string replacement) {
      var line = start.List;
      var character = start;
      end = end.Next;
      while(character != end) {
        var d = character;
        character = character.Next;
        line.Remove(d);
      }

      foreach(char rep in replacement)
        line.AddBefore(end, rep);
    }

    protected LinkedListNode<char> FindEnd(LinkedListNode<char> character, string search) {
      int i = 0;
      while(true) {
        if(character?.Value != search[i]) return null;
        i++;
        if(i >= search.Length) break;
        character = character.Next;
      }

      return character;
    }
  }
}
