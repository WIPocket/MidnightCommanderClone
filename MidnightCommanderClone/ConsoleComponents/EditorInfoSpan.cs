using System;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class EditorInfoSpan : Span {
    public string Path;
    public int Column;
    public bool Modified;
    public char? Character;
    public int Lines;
    public int TopOffset;
    public int TopLine;
    public int AtByte;
    public int Bytes;

    public EditorInfoSpan() {
      Background = ConsoleColor.DarkCyan;
      Foreground = ConsoleColor.Black;
      Text = "";
    }

    public void Clear() {
      Path = "";
      Column = 0;
      Modified = false;
      Lines = 0;
      TopOffset = 0;
      TopLine = 0;
      AtByte = 0;
      Bytes = 0;
    }

    public void Recalc() {
      if(Character == null && (TopOffset + TopLine + 1) < Lines)
        Character = '\n';
        
      Text = $"{Path}   [-{(Modified ? "M" : "-")}--]  {Column} L:[{String.Format("{0,4:###0}", TopOffset + 1)}+{String.Format("{0,3:##0}", TopLine)}  {String.Format("{0,4:###0}", (TopOffset + TopLine + 1))}/{String.Format("{0,4:###0}", Lines)}] {(Character == null ? "" : $"{String.Format("{0,4:0000}", (int) Character)} 0x{String.Format("{0,0:X}", (int) Character).PadLeft(3, '0')}")}";
    }
  }
}
