using System;
using System.Threading;

namespace MidnightCommanderClone {
  public class ConsoleUI {
    public ConsoleBuffer Buffer;
    public ConsoleLayout UI;

    public void Interactive() {
      Buffer = new ConsoleBuffer();
      UI = new UIContainer();

      Buffer.Resize(Console.WindowWidth, Console.WindowHeight);
      UI.Resize(Console.WindowWidth, Console.WindowHeight);
      UI.RenderBuffer = Buffer;
      UI.Render();
      Buffer.Flush();

      Keys.KeySet = new FileExplorerKeys();

      Thread resizeHandler = new Thread(HandleResize);
      resizeHandler.Start();
      Thread keyInputHandler = new Thread(HandleKeyInput);
      keyInputHandler.Start();
    }

    public void HandleKeyInput() {
      while(true) {
        UI.Render();
        Buffer.Flush();
        UI.HandleKeyPress(Console.ReadKey(true));
      }
    }

    public void HandleResize() {
      var lastWidth = 0;
      var lastHeight = 0;
      while(true) {
        var width = Console.WindowWidth;
        var height = Console.WindowHeight;
        if(width != lastWidth || height != lastHeight) {
          try {
            Buffer.Resize(Console.WindowWidth, Console.WindowHeight);
            UI.Resize(Console.WindowWidth, Console.WindowHeight);
            UI.Render();
            Buffer.Flush();
          } catch { }

          lastWidth = width;
          lastHeight = height;
        }
        Thread.Sleep(10);
      }
    }
  }

  public class UIContainer : ConsoleLayoutOverlayContainer {
    public ConsoleLayoutOverlay Main;
    public ConsoleLayoutOverlay Menu;
    public ConsoleLayoutOverlay Error;
    public ConsoleLayoutOverlay FileOperationOverlay;
    public UIFileOperation FileOperation;
    public ConfirmSavePopup ConfirmSave;
    public UISwitcher Switcher;
    public UIContainer() {
      Main = new ConsoleLayoutOverlay();
      Main.AddChild(new UIMain());
      AddChild(Main);
      Menu = new ConsoleLayoutOverlay() { Enabled = false };
      Menu.AddChild(new UIMenu());
      AddChild(Menu);
      FileOperationOverlay = new ConsoleLayoutOverlay() { Enabled = false };
      FileOperationOverlay.AddChild(FileOperation = new UIFileOperation());
      AddChild(FileOperationOverlay);
      var ConfirmSaveOverlay = new ConsoleLayoutOverlay() { Enabled = false };
      ConfirmSaveOverlay.AddChild(ConfirmSave = new ConfirmSavePopup());
      AddChild(ConfirmSaveOverlay);
      Error = new ConsoleLayoutOverlay() { Enabled = false };
      Error.AddChild(new UIError());
      AddChild(Error);
      Switcher = FindChildren<UISwitcher>()[0];
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Switcher.Current is UIFileExplorer) {
        if(Keys.Is(info, Keys.Menu))
          Menu.Enabled ^= true;
        else if(Keys.Is(info, Keys.Copy))
          FileOperation.Show<CopyDialog>();
        else if(Keys.Is(info, Keys.RenMov))
          FileOperation.Show<RenMovDialog>();
        else if(Keys.Is(info, Keys.Mkdir))
          FileOperation.Show<MkDirDialog>();
        else if(Keys.Is(info, Keys.Delete))
          FileOperation.Show<DeleteDialog>();
        else if(Keys.Is(info, Keys.Touch))
          FileOperation.Show<TouchDialog>();
        else try {
          base.HandleKeyPress(info);
        } catch (Exception e) {
          ShowError(e);
        }
      } else try {
        base.HandleKeyPress(info);
      } catch (Exception e) {
        ShowError(e);
      }
    }

    public override void Render() {
      try {
        base.Render();
      } catch (Exception e) {
        ShowError(e);
      }
    }

    public void ShowError(Exception e) {
      if(false) {
        Console.Clear();
        Console.ResetColor();
        throw new Exception("Thrown", e);
      }
      if (e.InnerException == null) e = new Exception("Error", e);
      Error.Enabled = true;
      UIError popup = (UIError) Error.Child;
      Box box = (Box)popup.Child;
      box.Title = $" {e.Message} ";
      Span message = (Span)box.Child;
      message.Text = e.InnerException.Message;
    }
  }

  public class UIError : ConsoleLayoutPopup {
    public UIError() {
      Background = ConsoleColor.DarkRed;
      TargetWidth = 40;
      TargetHeight = 10;
      var ErrBox = new Box() {
        Background = ConsoleColor.DarkRed,
        Foreground = ConsoleColor.White,
        BackgroundSelected = ConsoleColor.DarkRed,
        ForegroundSelected = ConsoleColor.Yellow,
        CenterTitle = true
      };
      ErrBox.AddChild(new WrappingSpan() { Background = ConsoleColor.DarkRed, Foreground = ConsoleColor.White });
      AddChild(ErrBox);
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      ((ConsoleLayoutOverlay)Parent).Enabled = false;
    }
  }

  public class UIMain : ConsoleLayoutHorizontal {
    public HelpLine HelpLine;
    public UIMain() {
      UISwitcher switcher;
      AddChild(switcher = new UISwitcher());
      AddChild(HelpLine = new HelpLine());
      switcher.Switch<UIFileExplorer, FileExplorerKeys>();
    }
  }

  public class UISwitcher : ConsoleLayoutSwitch {
    public UISwitcher() {
      TargetHeight = (Ratio) 1;

      AddChild(new UIFileExplorer());
      AddChild(new UIFileView());
      AddChild(new UIFileEditor());
    }

    public void Switch<TComponent, TKeySet> ()
      where TComponent : ConsoleComponent
      where TKeySet : KeySet, new()
    {
      Current = Children.Find(child => child is TComponent);
      Keys.KeySet = new TKeySet();
      FindParent<UIMain>().HelpLine.ShowHelp(Keys.KeySet.HelpLine);
    }
  }

  public class UIFileView : Box {
    WrappingSpan FileText;
    public UIFileView() {
      AddChild(FileText = new WrappingSpan());
    }

    public void ViewFile(string path) {
      Title = $" {path} ";
      string content = String.Join('\n', System.IO.File.ReadAllLines(path));
      FileText.Text = content;

      FindParent<UISwitcher>().Switch<UIFileView, FileViewKeys>();
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Quit))
        FindParent<UISwitcher>().Switch<UIFileExplorer, FileExplorerKeys>();
      else
        base.HandleKeyPress(info);
    }
  }

  public class UIFileExplorer : ConsoleLayoutVertical {
    public UIFileExplorer() {
      AddChild(new FilesystemView());
      AddChild(new FilesystemView());
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Quit)) {
        Console.CursorVisible = true;
        System.Environment.Exit(0);
      }
      else base.HandleKeyPress(info);
    }
  }

  public class UIMenu : ConsoleLayoutPopup {
    public UIMenu() {
      Background = ConsoleColor.DarkCyan;
      TargetWidth = 100;
      TargetHeight = 50;
      var box = new Box() { Background = Background, Foreground = ConsoleColor.White, Title = "User menu" };
      box.AddChild(new Input());
      AddChild(box);
    }
  }

  public class UIFileOperation : ConsoleLayoutPopup {
    public UIFileOperation() {
      Background = ConsoleColor.Gray;
      TargetWidth = 100;
      TargetHeight = 1;
      var box = new FileOperationBox() { Background = Background, Foreground = ConsoleColor.Black, CenterTitle = true };
      box.AddChild(new CopyDialog());
      AddChild(box);
    }

    public T Show<T>() where T : FileOperationDialog, new() {
      var box = (FileOperationBox) Child;
      box.Children = new System.Collections.Generic.List<ConsoleComponent>();
      T dialog = new T();
      box.AddChild(dialog);
      dialog.Show();
      box.Title = dialog.Title;
      TargetHeight = dialog.OuterHeight;
      Position = (0, 0);
      Resize(Parent.Width, Parent.Height);
      box.Resize(box.Width, box.Height);
      return dialog;
    }
  }

  public class UIFileEditor : ConsoleLayoutHorizontal {
    MultilineInput Editor;
    EditorInfoSpan Info;
    public UIFileEditor() {
      AddChild(Info = new EditorInfoSpan() { TargetHeight = 1 });
      AddChild(Editor = new MultilineInput() { TargetHeight = (Ratio) 1 });
      Editor.EditorInfo = Info;
      SelectedChildIndex = 1;
    }

    public void ViewFile(string path) {
      Info.Clear();
      Info.Path = path;
      string content = String.Join('\n', System.IO.File.ReadAllLines(path));
      Editor.Text = content;
      FindParent<UISwitcher>().Switch<UIFileEditor, FileEditorKeys>();
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Save))
        FindParent<UIContainer>().ConfirmSave.ConfirmSave(Info.Path, Editor.Text, false);
      else
        base.HandleKeyPress(info);
    }
  }
}
