using System;

namespace MidnightCommanderClone {
  public class ConsoleLayoutVertical : ConsoleLayout {
    public override void Resize(Characters width, Characters height) {
      base.Resize(width, height);
      var targets = new ConsoleUnit[Children.Count];
      for(int i = 0; i < Children.Count; i++)
      targets[i] = Children[i].TargetWidth;
      var widths = ConsoleLayout.CollapseToCharacters(targets, Width);
      int posOffset = 0;
      for(int i = 0; i < widths.Length; i++) {
        Children[i].Position = (this.Position.x + posOffset, this.Position.y);
        Children[i].Resize(widths[i], this.Height);
        posOffset += widths[i];
      }
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Tab)) {
        SelectedChildIndex++;
        SelectedChildIndex %= Children.Count;
      } else base.HandleKeyPress(info);
    }
  }

  public class ConsoleLayoutHorizontal : ConsoleLayout {
    public override void Resize(Characters width, Characters height) {
      base.Resize(width, height);
      var targets = new ConsoleUnit[Children.Count];
      for(int i = 0; i < Children.Count; i++)
      targets[i] = Children[i].TargetHeight;
      var heights = ConsoleLayout.CollapseToCharacters(targets, Height);
      int posOffset = 0;
      for(int i = 0; i < heights.Length; i++) {
        Children[i].Position = (this.Position.x, this.Position.y + posOffset);
        Children[i].Resize(this.Width, heights[i]);
        posOffset += heights[i];
      }
    }
  }

  public class CharacterComponent : ConsoleComponent {
    ConsoleCharacter character;

    public CharacterComponent(ConsoleCharacter character) {
      TargetWidth = 1;
      TargetHeight = 1;
      this.character = character;
    }

    public CharacterComponent() {
      TargetWidth = 1;
      TargetHeight = 1;
      this.character = new ConsoleCharacter('│');
    }

    public override void Render() {
      Place(0, 0, character);
    }
  }
}
