using System;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class ConsoleLayoutOverlayContainer : ConsoleLayout {
    ConsoleComponent fakeSelected;
    public override ConsoleComponent SelectedChild {
      get {
        if (fakeSelected != null) return fakeSelected;
        for(int i = Children.Count - 1; i >= 0; i--)
          if(((ConsoleLayoutOverlay)Children[i]).Enabled) return Children[i];
        return null;
      }
    }

    public override void Resize(Characters width, Characters height) {
      Width = width;
      Height = height;
      foreach (ConsoleLayoutOverlay child in Children) {
        child.Position = Position;
        child.Resize(width, height);
      }
    }

    public override void Render() {
      foreach(var child in Children) {
        fakeSelected = child;
        child.Render();
      }
      fakeSelected = null;
    }
  }

  public class ConsoleLayoutOverlay : ConsoleLayout {
    public bool Enabled = true;

    public override void Render() {
      if(Enabled)
        base.Render();
    }

    public override void Resize(Characters width, Characters height) {
      Width = width;
      Height = height;
      Child.Position = Position;
      Child.Resize(width, height);
    }
  }
}
