using System;

namespace MidnightCommanderClone {
  public class ConsoleLayoutPopup : ConsoleLayout {
    public ConsoleColor Shadow = ConsoleColor.Black;
    public ConsoleColor Background = ConsoleColor.Gray;

    public override void Render() {
      for(int y = 0; y < Height; y++)
        for(int x = 0; x < Width; x++)
          Place(x, y, new ConsoleCharacter(' ', ConsoleColor.Black, Background));

      for(int x = 1; x < Width + 2; x++)
        Place(x, Height, new ConsoleCharacter(' ', ConsoleColor.Black, Shadow));

      for(int y = 1; y < Height; y++) {
        Place(Width, y, new ConsoleCharacter(' ', ConsoleColor.Black, Shadow));
        Place(Width + 1, y, new ConsoleCharacter(' ', ConsoleColor.Black, Shadow));
      }

      Child.Render();
    }

    public override void Resize(Characters width, Characters height) {
      Width = Math.Min((Characters) TargetWidth, width - 10);
      Height = Math.Min((Characters) TargetHeight, height - 10);
      Position = (
        Position.x + (width / 2 - Width / 2),
        Position.y + (height / 2 - Height / 2)
      );

      Child.Position = (Position.x + 1, Position.y + 1);
      Child.Resize(Width - 2, Height - 2);
    }
  }
}
