using System;
using System.Linq;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public abstract class ConsoleLayout : ConsoleComponent {
    public List<ConsoleComponent> Children = new List<ConsoleComponent>();
    protected int SelectedChildIndex = 0;

    public virtual ConsoleComponent SelectedChild {
      get {
        if(SelectedChildIndex < 0)
          return null; 
        if(Children.Count > 0)
          return Children[SelectedChildIndex];
        return null;
      }
    }

    public virtual ConsoleComponent Child {
      get {
        return Children[0];
      }
    }

    public virtual void AddChild(ConsoleComponent child) {
      Children.Add(child);
      child.Parent = this;
    }

    public static Characters[] CollapseToCharacters(ConsoleUnit[] units, Characters targetTotalSize) {
      int sumOfRatios = units.Sum(unit => unit.Unit == ConsoleUnits.Ratio ? unit.Size : 0);
      int sumOfCharacters = units.Sum(unit => unit.Unit == ConsoleUnits.Characters ? unit.Size : 0);
      int freeSpace = targetTotalSize - sumOfCharacters;
      int perRatio = freeSpace / (sumOfRatios > 0 ? sumOfRatios : 1);

      Characters[] collapsed = new Characters[units.Length];
      for (int i = 0; i < units.Length; i++)
        collapsed[i] = units[i] is Characters ? units[i].Size : perRatio * units[i].Size;

      return collapsed;
    }

    public override void Render() {
      foreach(var child in Children)
        child.Render();
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(SelectedChild != null)
        SelectedChild.HandleKeyPress(info);
    }

    public virtual List<T> FindChildren<T>() where T : ConsoleComponent {
      List<T> result = new List<T>();
      foreach(var child in Children) {
        if(child is T) result.Add((T)child);
        if(child is ConsoleLayout) result.AddRange(((ConsoleLayout)child).FindChildren<T>());
      }
      return result;
    }
  }
}
