using System;
using System.Collections.Generic;

namespace MidnightCommanderClone {
  public class ConsoleLayoutDialog : ConsoleLayoutHorizontal {
    public int SelectedElement = 0;
    public List<DialogElement> Elements;
    protected int DefaultAction;

    public override void Render() {
      SelectedChildIndex = -1;
      if(Elements == null) {
        Elements = FindChildren<DialogElement>();
        for(int i = 0; i < Elements.Count; i++) {
          Elements[i].ElementID = i;
          if(Elements[i].DefaultSelect) SelectedElement = i;
          if(Elements[i].DefaultAction) DefaultAction = i;
        }
      }

      base.Render();
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      if(Keys.Is(info, Keys.Tab)) {
        SelectedElement++;
        SelectedElement %= Elements.Count;
      } else Elements[SelectedElement].HandleKeyPress(info);
    }

    public void DoDefaultAction() {
      Elements[DefaultAction].Use();
    }
  }

  public abstract class DialogElement : ConsoleComponent {
    public bool DefaultSelect = false;
    public bool DefaultAction = false;
    public int TabOrder = 0;
    public int ElementID = 0;

    public new bool Selected {
      get {
        var dialog = FindParent<ConsoleLayoutDialog>();
        if (dialog == null)
          return base.Selected;
        return dialog.SelectedElement == ElementID;
      }
    }

    public Action Use;
  }
}
