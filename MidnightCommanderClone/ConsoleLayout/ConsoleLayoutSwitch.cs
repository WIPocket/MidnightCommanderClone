using System;

namespace MidnightCommanderClone {
  public class ConsoleLayoutSwitch : ConsoleLayout {
    public ConsoleComponent Current;
    public override void Render() {
      Current.Render();
    }

    public override void Resize(Characters width, Characters height) {
      base.Resize(width, height);
      foreach(var child in Children)
        child.Resize(width, height);
    }

    public override void HandleKeyPress(ConsoleKeyInfo info) {
      Current.HandleKeyPress(info);
    }
  }
}
